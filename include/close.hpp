#ifndef CLOSE_HPP
#define CLOSE_HPP

namespace close_door
{

void player_try_close_or_jam();

} // close_door

#endif // CLOSE_HPP
